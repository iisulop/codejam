-module(counting_sheep).
-export([count/1]).
-export([get_last_sheep_num/6]).
-export([gather_results/2]).

count(Filename) ->
  {ok, Device} = file:open(Filename, [read]),
  handle_input(Device).


handle_input(Device) ->
  {Count, _Rest} = string:to_integer(io:get_line(Device, "")),
  ResultProc = spawn(?MODULE, gather_results, [[], Count]),
  handle_case(Device, ResultProc, Count, 1).


handle_case(Device, ResultProc, Count, Acc) when Acc =< Count ->
  {N, _Rest} = string:to_integer(io:get_line(Device, "")),
  spawn(?MODULE, get_last_sheep_num, [ResultProc, Acc, N, 1, sets:new(), 0]),
  NewAcc = Acc + 1,
  handle_case(Device, ResultProc, Count, NewAcc);
handle_case(_Device, _ResultProc, Count, Acc) when Acc > Count ->
  ok.


get_last_sheep_num(ResultProc, Case, N, _I, _Numbers, _Size) when N == 0 ->
  ResultProc ! {Case, insomnia};
get_last_sheep_num(ResultProc, Case, N, I, _Numbers, Size) when Size == 10 ->
  ResultProc ! {Case, N * (I - 1)};
get_last_sheep_num(ResultProc, Case, N, I, Numbers, Size) when Size < 10 ->
  CurrentNumbers = sets:from_list(integer_to_list(N * I)),
  NewNumbers = sets:union(Numbers, CurrentNumbers),
  NewI = I + 1,
  get_last_sheep_num(ResultProc, Case, N, NewI, NewNumbers, sets:size(NewNumbers)).


gather_results(Results, Count) when length(Results) < Count ->
  NewResults = receive
                 {N, LastSheep} -> [{N, LastSheep}] ++ Results
               end,
  gather_results(NewResults, Count);
gather_results(Results, _Count) ->
  SortedResults = lists:sort(Results),
  print_results(SortedResults).


print_results([{Case, insomnia}|Rest]) ->
  io:format("Case #~p: INSOMNIA~n", [Case]),
  print_results(Rest);
print_results([{Case, Result}|Rest]) ->
  io:format("Case #~p: ~p~n", [Case, Result]),
  print_results(Rest);
print_results([]) ->
  ok.
